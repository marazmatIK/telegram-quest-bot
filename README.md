# What is this?
Simple Telegram Quest Bot for awesome text adventures
# How to build
Just perform in the root of the project:
````
mvn clean install
````
or
````
mvn clean install -DskipTests=true
````
if you're naughty developer ;)
# How to run
1. If your network isn't blocked by bad guys just execute in the /target folder:
    ````
    java -jar -Dtg.bot.token=$YOUR_BOT_TOKEN quest-tg-bot-0.0.1-SNAPSHOT.jar
    ````
2. If your network IS blocked by bad guys, you need some proxy server forwarding outbound/inbound requests (port and host); execute in the `/target` folder:
    ````
    java -jar -Dtg.bot.token=$YOUR_BOT_TOKEN -Dtg.bot.enable.proxy=true -Dtg.bot.proxy.host=$HTTP_PROXY_HOST -Dtg.bot.proxy.port=$HTTP_PROXY_PORT quest-tg-bot-0.0.1-SNAPSHOT.jar
    ````

Enjoy!
