package com.pc.questbot.app.services;

import com.pc.questbot.app.domain.Quest;

public interface QuestLoader {
    Quest loadQuest();
}
