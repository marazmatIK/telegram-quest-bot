package com.pc.questbot.app.services;

import com.pc.questbot.app.domain.Quest;
import com.pc.questbot.app.domain.Stage;
import com.pc.questbot.app.domain.Transition;
import com.pc.questbot.app.tg.UserContextStorage;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public abstract class AbstractQuestEngine implements QuestEngine {
    private Quest quest;
    private final QuestLoader questLoader;
    private final UserContextStorage contextStorage;
    private final Long identity;

    public AbstractQuestEngine(QuestLoader questLoader, UserContextStorage contextStorage, Long identity) {
        this.questLoader = questLoader;
        this.contextStorage = contextStorage;
        this.identity = identity;
    }

    @Override
    public void init() {
        quest = questLoader.loadQuest();
    }

    @Override
    public void start() {
        init();
        Stage stage = quest.startingStage();
        contextStorage.updateCurrentStage(identity, stage);
        process(stage, null, identity);
    }

    @Override
    public Stage processTransition(Transition transition) {
        Stage stage = quest.findById(transition.getStageId());
        contextStorage.updateCurrentStage(identity, stage);
        process(stage, transition, identity);
        if(stage.isFinish()) {
            onEnd();
        }
        return stage;
    }

    protected abstract void process(Stage stage, Transition fromTransition, Long identity);

    @Override
    public Transition translate(int number) {
        return contextStorage.getCurrentStage(identity).getTransitions().stream().filter(transition -> transition.getNumber() == number).findFirst().get();
    }

    @Override
    public void onEnd() {
        onEnd(identity);
    }

    protected abstract void onEnd(Long identity);

    public void onNextStep(int choiceNumber) {
        Transition transition = translate(choiceNumber);
        log.info("User choice: {} (actionText = {}, nextStageId = {})", choiceNumber, transition.getActionText(), transition.getStageId());
        processTransition(transition);
    }
}
