package com.pc.questbot.app.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.pc.questbot.app.domain.Quest;
import com.pc.questbot.app.domain.Stage;
import com.pc.questbot.app.domain.Transition;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.io.Writer;
import java.util.Arrays;

@Component
public class JsonFileQuestLoader implements QuestLoader {
    private static final String QUEST_JSON_FILE_PATH = "/quest.json";
    private Quest quest;

    @Override
    public synchronized Quest loadQuest() {
        if (quest == null) {
            ObjectMapper mapper = new ObjectMapper();
            InputStream is = JsonFileQuestLoader.class.getResourceAsStream(QUEST_JSON_FILE_PATH);
            try {
                quest = mapper.readValue(is, Quest.class);
                return quest;
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
        return quest;
    }

    public static void main(String[] args) throws IOException {
        QuestLoader questLoader = new JsonFileQuestLoader();
        Quest quest = questLoader.loadQuest();

        assert quest.getStages().size() == 3;

        /*Stage stageZero = new Stage();
        stageZero.setText("text");
        stageZero.setFinish(false);
        stageZero.setId(0L);
        stageZero.setImagePath("img.path");
        stageZero.setImageUrl("img.com/img.jpg");

        Stage stageA = new Stage();
        stageA.setId(2L);
        stageA.setImageUrl("url-url.com");
        stageA.setImagePath("img/image.png");
        stageA.setText("I'm a stage A");
        stageA.setFinish(false);

        Stage stageB = new Stage();
        stageB.setId(3L);
        stageB.setImageUrl("B-image.com");
        stageB.setImagePath("img/imageB.png");
        stageB.setText("I'm a stage B");
        stageB.setFinish(true);

        Transition transitionA = new Transition();
        transitionA.setLongText("longLongText");
        transitionA.setShortText("short");
        transitionA.setNumber(1);
        transitionA.setActionText("actionA");
        transitionA.setStageId(stageA.getId());

        Transition transitionB = new Transition();
        transitionB.setLongText("longLongText2");
        transitionB.setShortText("short2");
        transitionB.setNumber(2);
        transitionB.setActionText("actionB");
        transitionB.setStageId(stageB.getId());

        stageZero.setTransitions(Arrays.asList(transitionA, transitionB));

        Quest quest = new Quest(Arrays.asList(stageZero, stageA, stageB));

        ObjectMapper mapper = new ObjectMapper();
        Writer writer = new StringWriter();
        mapper.writer().writeValue(writer, quest);

        System.out.println(writer.toString());*/
    }
}
