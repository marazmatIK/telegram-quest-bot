package com.pc.questbot.app.services;

import com.pc.questbot.app.domain.Stage;
import com.pc.questbot.app.domain.Transition;
import com.pc.questbot.app.tg.UserContextStorage;

import java.util.InputMismatchException;
import java.util.Random;
import java.util.Scanner;

public class ConsoleQuestEngine extends AbstractQuestEngine {
    private final Scanner scanner = new Scanner(System.in);

    private static final String GAME_OVER = "Game Over =/";

    public ConsoleQuestEngine(QuestLoader questLoader, UserContextStorage contextStorage, Long identity) {
        super(questLoader, contextStorage, identity);
    }

    @Override
    protected void process(Stage stage, Transition fromTransition, Long identity) {
        if (fromTransition != null) {
            System.out.println(fromTransition.getActionText());
            System.out.println("-----------------------------");
        }
        System.out.println(stage.getText());
        System.out.println("Your actions: ");
        System.out.println("_____________________________");
        if (stage.getTransitions() != null) {
            stage.getTransitions().forEach(transition -> {
                System.out.println("  (" + transition.getNumber() + ") " + transition.getShortText());
                System.out.println("    - " + transition.getLongText());
            });
        } else {
            System.out.println(". . .");
        }
        while (true) {
            int choice = scanner.nextInt();
            if (stage.getTransitions().stream().filter(transition -> transition.getNumber() == choice).count() > 0) {
                onNextStep(choice);
            } else {
                System.out.println("Not valid option.. Please, try again.");
            }
        }
    }

    @Override
    protected void onEnd(Long identity) {
        System.out.println();
        System.out.println(GAME_OVER);
        scanner.close();
    }

    public static void main(String[] args) {
        Random r = new Random();
        System.out.println(r.nextBoolean());
        QuestEngine questEngine = new ConsoleQuestEngine(new JsonFileQuestLoader(), new UserContextStorage(), -100500L);
        questEngine.start();
    }
}
