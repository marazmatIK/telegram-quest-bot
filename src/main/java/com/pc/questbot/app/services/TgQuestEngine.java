package com.pc.questbot.app.services;

import com.pc.questbot.app.domain.Stage;
import com.pc.questbot.app.domain.Transition;
import com.pc.questbot.app.tg.UserContextStorage;
import com.pc.questbot.app.tg.cmd.CommandsNamespace;
import lombok.extern.slf4j.Slf4j;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.methods.send.SendPhoto;
import org.telegram.telegrambots.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import org.telegram.telegrambots.bots.AbsSender;
import org.telegram.telegrambots.exceptions.TelegramApiException;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import static com.pc.questbot.app.tg.cmd.ReplyMarkupUtils.button;
import static org.apache.commons.lang3.StringUtils.isNotEmpty;

@Slf4j
public class TgQuestEngine extends AbstractQuestEngine {

    private final AbsSender bot;

    private static final String TARGET_USER_PROPS_PATH = "target.properties";

    private ClassLoader loader = Thread.currentThread().getContextClassLoader();
    private final Properties props = new Properties();

    {
        try(InputStream resourceStream = loader.getResourceAsStream(TARGET_USER_PROPS_PATH)) {
            props.load(new InputStreamReader(resourceStream, "UTF-8"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public TgQuestEngine(QuestLoader questLoader, UserContextStorage contextStorage, Long chatId, AbsSender bot) {
        super(questLoader, contextStorage, chatId);
        this.bot = bot;
    }

    @Override
    protected void process(Stage stage, Transition fromTransition, Long chatId) {
        log.info("User {} transited to stage {} via transition {}", chatId, stage.getId(), fromTransition == null ? "null" : fromTransition.getNumber());
        SendMessage answerMessage = new SendMessage();
        answerMessage.setText(buildStageMessage(stage, fromTransition));
        answerMessage.setParseMode("HTML");
        answerMessage.setChatId(chatId);
        embedChoiceButtons(answerMessage, stage);
        try {
            if (isNotEmpty(stage.getImagePath()) || isNotEmpty(stage.getImageUrl())) {
                sendPhoto(stage, chatId);
            }
            bot.execute(answerMessage);
        } catch (TelegramApiException e) {
            log.error("Error processing stage {}: {}", stage.getId(), e.getMessage(), e);
        }
    }

    @Override
    protected void onEnd(Long chatId) {
        log.info("User {} finished quest", chatId);
        /*assert chatId != null;
        log.info("User {} finished quest", chatId);
        SendMessage answerMessage = new SendMessage();
        answerMessage.setText(finishMessage());
        answerMessage.setParseMode("HTML");
        answerMessage.setChatId(chatId);
        try {
            bot.execute(answerMessage);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }*/
    }

    private String buildStageMessage(Stage stage, Transition fromTransition) {
        StringBuilder sb = new StringBuilder();
        if (fromTransition != null && isNotEmpty(fromTransition.getActionText())) {
            sb.append("<i>").append(fromTransition.getActionText()).append("</i>\n\n");
        }
        if(isNotEmpty(stage.getText())) {
            sb.append(stage.getText()).append("\n");
        }
        if(!stage.isFinish()) {
            sb.append("\nВаши действия:\n");
        }
        return sb.toString();
    }

    private void sendPhoto(Stage stage, Long chatId) throws TelegramApiException {
        if (isNotEmpty(stage.getImageUrl())) {
            log.info("Sending photo {} to user {} for stage {}", stage.getImageUrl(), chatId, stage.getId());
            try {
                SendPhoto sendPhoto = new SendPhoto();
                sendPhoto.setChatId(chatId);
                sendPhoto.setPhoto(stage.getImageUrl());
                bot.sendPhoto(sendPhoto);
            } catch (Throwable ex) {
                log.error("Error sending photo file {} to user {} for stage {}: {}", stage.getImageUrl(),
                        chatId,
                        stage.getId(),
                        ex.getMessage(),
                        ex);
            }
        } else {
            log.info("Sending photo {} to user {} for stage {}", stage.getImagePath(), chatId, stage.getId());
            ClassLoader loader = Thread.currentThread().getContextClassLoader();
            try {
                URL imageUrl = loader.getResource(stage.getImagePath());
                File imgFile = new File(imageUrl.getFile());
                SendPhoto sendPhoto = new SendPhoto();
                sendPhoto.setChatId(chatId);
                sendPhoto.setNewPhoto(imgFile);
                bot.sendPhoto(sendPhoto);
            } catch (Throwable ex) {
                log.error("Error sending photo file {} to user {} for stage {}: {}", stage.getImagePath(),
                        chatId,
                        stage.getId(),
                        ex.getMessage(),
                        ex);
            }
        }
    }

    private void embedChoiceButtons(SendMessage message, Stage stage) {
        if(!stage.isFinish() && (stage.getTransitions() == null || stage.getTransitions().isEmpty())) {
            return;
        }
        List<List<InlineKeyboardButton>> keyboard = new ArrayList<>();
        InlineKeyboardMarkup markup = new InlineKeyboardMarkup();
        StringBuilder sb = new StringBuilder();
        stage.getTransitions().forEach(transition -> {
            sb.append("<b>").append(transition.getNumber()).append(":</b> ").append(transition.getShortText()).append("\n");
            keyboard.add(Arrays.asList(button(transition.getNumber() + ": " + transition.getShortText(), "/" + CommandsNamespace.CMD_EXEC_STEP + " " + transition.getNumber())));
        });
        if (stage.isFinish()) {
            // Adding reset quest button finally
            keyboard.add(Arrays.asList(button("Начать заново", "/" + CommandsNamespace.CMD_RESTART)));
        }

        markup.setKeyboard(keyboard);
        message.setText(message.getText() + "\n" + sb.toString());
        message.setReplyMarkup(markup);
    }

    private String finishMessage() {
        return props.getProperty("tg.finish.message");
    }
}
