package com.pc.questbot.app.services;

import com.pc.questbot.app.domain.Stage;
import com.pc.questbot.app.domain.Transition;

public interface QuestEngine {
    void init();
    void start();
    Stage processTransition(Transition transition);
    Transition translate(int choiceNumber);
    void onEnd();
    void onNextStep(int choiceNumber);
}
