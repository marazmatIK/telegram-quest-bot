package com.pc.questbot.app;

import com.pc.questbot.app.tg.Bot;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.telegram.telegrambots.ApiContextInitializer;
import org.telegram.telegrambots.TelegramBotsApi;
import org.telegram.telegrambots.exceptions.TelegramApiRequestException;
import org.telegram.telegrambots.generics.LongPollingBot;
import org.telegram.telegrambots.bots.DefaultBotOptions;
import org.apache.http.client.config.RequestConfig;
import org.telegram.telegrambots.ApiContext;
import org.apache.http.HttpHost;

import javax.annotation.PostConstruct;

@SpringBootApplication(scanBasePackages = "com.pc.questbot.app")
public class QuestBotApp {

    @Value("${tg.bot.username}")
    private String botUsername;

    @Value("${tg.bot.token}")
    private String token;

    @Value("${tg.bot.enable.proxy:false}")
    private boolean isProxyEnabled;

    @Value("${tg.bot.proxy.host:}")
    private String proxyHost;

    @Value("${tg.bot.proxy.port:1080}")
    private int proxyPort;

    public static void main(String[] args) {
        ApplicationContext context = SpringApplication.run(QuestBotApp.class, args);
        context.getBean(LongPollingBot.class);


    }

    @PostConstruct
    void init() throws TelegramApiRequestException {
    }

    @Bean
    public LongPollingBot getBot() throws TelegramApiRequestException {
        ApiContextInitializer.init();

        DefaultBotOptions opts = ApiContext.getInstance(DefaultBotOptions.class);
        if (isProxyEnabled) {
            RequestConfig config = RequestConfig
                .custom()
                .setProxy(new HttpHost(proxyHost, proxyPort))
                .build();
            opts.setRequestConfig(config);
        }
        LongPollingBot bot = new Bot(botUsername, token, opts);

        TelegramBotsApi botsApi = new TelegramBotsApi();

        botsApi.registerBot(bot);

        return bot;
    }
}
