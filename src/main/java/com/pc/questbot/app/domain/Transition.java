package com.pc.questbot.app.domain;

import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@NoArgsConstructor
public class Transition {
    private int number;
    private String longText;
    private String shortText;

    private String actionText;

    private long stageId;
    public int getNumber() {
        return number;
    }

    public String getLongText() {
        return longText;
    }

    public String getShortText() {
        return shortText;
    }

    public long getStageId() {
        return stageId;
    }

    public String getActionText() {
        return actionText;
    }

    public void setActionText(String actionText) {
        this.actionText = actionText;
    }

    public Transition(int number, String longText, String shortText, String actionText, long stageId) {
        this.number = number;
        this.longText = longText;
        this.shortText = shortText;
        this.stageId = stageId;
        this.actionText = actionText;
    }
}
