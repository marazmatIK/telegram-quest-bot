package com.pc.questbot.app.domain;

import lombok.*;

import java.util.List;

@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Quest {
    @Getter
    private List<Stage> stages;

    public Stage startingStage() {
        return findById(0L);
    }

    public Stage findById(long id) {
        return stages.stream().filter(stage -> stage.getId() == id).findFirst().get();
    }
}
