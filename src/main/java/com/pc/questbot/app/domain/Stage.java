package com.pc.questbot.app.domain;

import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Setter
@NoArgsConstructor
public class Stage {
    private long id;
    private String text;
    private String imageUrl;
    private String imagePath;
    private List<Transition> transitions;
    private boolean finish;

    public List<Transition> getTransitions() {
        return transitions;
    }

    public Stage(long id, String text, String imageUrl, String imagePath, List<Transition> transitions) {
        this.id = id;
        this.text = text;
        this.imageUrl = imageUrl;
        this.imagePath = imagePath;
        this.transitions = transitions;
    }

    public long getId() {
        return id;
    }

    public String getText() {
        return text;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getImagePath() {
        return imagePath;
    }

    public boolean isFinish() {
        return finish;
    }
}
