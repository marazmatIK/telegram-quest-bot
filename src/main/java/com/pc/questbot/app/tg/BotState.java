package com.pc.questbot.app.tg;

public enum BotState {
    BEFORE_START,
    INIT_USER,
    REGULAR_MODE,
    QUEST_FINISHED
}
