package com.pc.questbot.app.tg.cmd;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateParseUtils {
    private static final DateFormat WHITESPACED_DATE = new SimpleDateFormat("d M yyyy");
    private static final DateFormat HYPHEN_DATE = new SimpleDateFormat("d-M-yyyy");
    private static final DateFormat DOT_DATE = new SimpleDateFormat("d.M.yyyy");
    private static final DateFormat SLASH_DATE = new SimpleDateFormat("d/M/yyyy");

    static Date parseStartDate(String[] args) throws IllegalArgumentException {
        try {
            return WHITESPACED_DATE.parse(String.join(" ", args[0], args[1], args[2]));
        } catch (Exception ex) {
            throw new IllegalArgumentException("Wrong command format: " + ex.getMessage(), ex);
        }
    }

    static Long parseExpenseTemplateId(String[] args) throws IllegalArgumentException {
        try {
            if (args[args.length - 1].startsWith("id:")) {
                return Long.parseLong(args[args.length - 1].substring(3));
            }
        } catch (Exception ex) {
            return null;
        }
        return null;
    }

    static Date parseEndDate(String[] args) throws IllegalArgumentException {
        try {
            return WHITESPACED_DATE.parse(String.join(" ", args[3], args[4], args[5]));
        } catch (Exception ex) {
            throw new IllegalArgumentException("Wrong command format: " + ex.getMessage(), ex);
        }
    }

    static Date parseDate(String stringDate) throws IllegalArgumentException {
        try {
            return WHITESPACED_DATE.parse(stringDate);
        } catch (Exception ex) {
            throw new IllegalArgumentException("Wrong command format: " + ex.getMessage(), ex);
        }
    }
}
