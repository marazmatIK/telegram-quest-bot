package com.pc.questbot.app.tg.cmd;

import com.pc.questbot.app.services.QuestEngine;
import com.pc.questbot.app.services.QuestLoader;
import com.pc.questbot.app.tg.UserContextStorage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.api.objects.Chat;
import org.telegram.telegrambots.api.objects.User;
import org.telegram.telegrambots.bots.AbsSender;
import org.telegram.telegrambots.exceptions.TelegramApiException;

import java.io.IOException;

@Component(CommandsNamespace.CMD_RESTART)
//TODO: are we really need Prototype scope for Bot Commands?..
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class RestartQuestCommand extends AbstractBotCommand {

    @Autowired
    private UserContextStorage userContextStorage;

    @Autowired
    private QuestLoader questLoader;

    public RestartQuestCommand() {
        super(CommandsNamespace.CMD_RESTART, CommandsNamespace.CMD_RESTART_DESC);
    }

    @Override
    protected void doExecute(AbsSender absSender, User user, Chat chat, String[] args) throws TelegramApiException, IOException {
        synchronized (userContextStorage) {
            QuestEngine engine = userContextStorage.getQuest(chat.getId());
            if (engine != null) {
                engine.start();
            }
        }
    }
}
