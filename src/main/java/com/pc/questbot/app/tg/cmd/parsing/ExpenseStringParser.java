package com.pc.questbot.app.tg.cmd.parsing;

import org.apache.commons.lang3.tuple.MutableTriple;
import org.apache.commons.lang3.tuple.Triple;

import java.math.BigDecimal;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

public class ExpenseStringParser {

    private static final Pattern REGEX = Pattern.compile("([\\W\\s\\n]+)([0-9]+([.][0-9]+)?)[.\\s]+([\\W\\s\\n]+)?");

    public static Triple<String, BigDecimal, String> parseArgs(String[] args) throws IllegalArgumentException {
        MutableTriple<String, BigDecimal, String> data = new MutableTriple<>();
        Matcher matcher = REGEX.matcher(String.join(" ", args));
        if (matcher.find()) {
            data.setLeft(matcher.group(1).trim());
            try {
                data.setMiddle(new BigDecimal(matcher.group(2)));
            } catch (NullPointerException ex) {
                throw new IllegalArgumentException("Wrong expense string format");
            }
            if (isNotBlank(matcher.group(4))) {
                data.setRight(matcher.group(4).replace('\n', '\u0000').trim());
            }
            if (isBlank(data.getLeft()) || ".".equals(data.getLeft()) ) {
                throw new IllegalArgumentException("Wrong expense string format");
            }
        } else {
            throw new IllegalArgumentException("Wrong expense string format");
        }

        return data;
    }
}
