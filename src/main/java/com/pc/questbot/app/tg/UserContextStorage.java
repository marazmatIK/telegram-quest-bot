package com.pc.questbot.app.tg;

import com.pc.questbot.app.domain.Stage;
import com.pc.questbot.app.services.QuestEngine;
import org.springframework.stereotype.Component;

import java.util.concurrent.ConcurrentHashMap;
import java.util.function.BiFunction;

@Component
public class UserContextStorage {
    private final ConcurrentHashMap<Long, BotState> userContext = new ConcurrentHashMap<>();
    private final ConcurrentHashMap<Long, Stage> userCurrentStage = new ConcurrentHashMap<>();
    private final ConcurrentHashMap<Long, QuestEngine> userQuests = new ConcurrentHashMap<>();


    public void updateState(Long chatId, BiFunction<Long, BotState, BotState> computeFunc) {
        userContext.compute(chatId, computeFunc);
    }

    public BotState getState(Long chatId) {
        return userContext.get(chatId);
    }

    public void updateCurrentStage(Long chatId, Stage stage) {
        userCurrentStage.put(chatId, stage);
    }

    public Stage getCurrentStage(Long chatId) {
        return userCurrentStage.get(chatId);
    }

    public synchronized void initQuestIfNeeded(Long chatId, QuestEngine questEngine) {
        QuestEngine prev = userQuests.putIfAbsent(chatId, questEngine);
        if (prev == null) {
            questEngine.start();
        }

    }

    public QuestEngine getQuest(Long chatId) {
        return userQuests.get(chatId);
    }
}
