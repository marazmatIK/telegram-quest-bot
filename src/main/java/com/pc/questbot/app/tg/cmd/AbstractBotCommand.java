package com.pc.questbot.app.tg.cmd;

import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.Chat;
import org.telegram.telegrambots.api.objects.User;
import org.telegram.telegrambots.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import org.telegram.telegrambots.bots.AbsSender;
import org.telegram.telegrambots.bots.commandbot.commands.BotCommand;
import org.telegram.telegrambots.exceptions.TelegramApiException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.pc.questbot.app.tg.cmd.ReplyMarkupUtils.button;

public abstract class AbstractBotCommand extends BotCommand {

    public AbstractBotCommand(String commandIdentifier, String description) {
        super(commandIdentifier, description);
    }

    @Override
    public void execute(AbsSender absSender, User user, Chat chat, String[] args) {
        try {
            doExecute(absSender, user, chat, args);
        } catch (TelegramApiException e) {
            //TODO: log error here
            e.printStackTrace();
        } catch (IOException e) {
            //TODO: send some SorryWeGotAnErrorMessage
            e.printStackTrace();
        }
    }

    SendMessage htmlFormatted(String text, Chat chat) {
        assert chat != null;
        return htmlFormatted(text, chat.getId());
    }

    SendMessage htmlFormatted(String text, Long chatId) {
        assert chatId != null;
        SendMessage answerMessage = new SendMessage();
        answerMessage.setText(text);
        answerMessage.setParseMode("HTML");
        answerMessage.setChatId(chatId);

        return answerMessage;
    }

    protected abstract void doExecute(AbsSender absSender, User user, Chat chat, String[] args) throws TelegramApiException,
            IOException;

    void addExitToHomeButton(SendMessage sendMessage) {
        List<List<InlineKeyboardButton>> keyboard = new ArrayList<>();
        InlineKeyboardMarkup markup = new InlineKeyboardMarkup();
        keyboard.add(Arrays.asList(ReplyMarkupUtils.button("Вернуться в основное меню", "/" + CommandsNamespace.CMD_EXIT_TO_MAIN_MENU)));

        markup.setKeyboard(keyboard);
        sendMessage.setReplyMarkup(markup);
    }
}
