package com.pc.questbot.app.tg.cmd;

import com.pc.questbot.app.services.QuestEngine;
import com.pc.questbot.app.tg.UserContextStorage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.api.objects.Chat;
import org.telegram.telegrambots.api.objects.User;
import org.telegram.telegrambots.bots.AbsSender;
import org.telegram.telegrambots.exceptions.TelegramApiException;

import java.io.IOException;

import static com.pc.questbot.app.tg.cmd.CommandsNamespace.CMD_EXEC_STEP;
import static com.pc.questbot.app.tg.cmd.CommandsNamespace.CMD_EXEC_STEP_DESC;

@Component(CMD_EXEC_STEP)
//TODO: are we really need Prototype scope for Bot Commands?..
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ExecStepCommand extends AbstractBotCommand {

    @Autowired
    private UserContextStorage userContextStorage;

    public ExecStepCommand() {
        super(CMD_EXEC_STEP, CMD_EXEC_STEP_DESC);
    }

    @Override
    protected void doExecute(AbsSender absSender, User user, Chat chat, String[] args) throws TelegramApiException, IOException {
        QuestEngine engine = userContextStorage.getQuest(chat.getId());
        if (engine != null) {
            engine.onNextStep(Integer.parseInt(args[0]));
        }
    }
}
