package com.pc.questbot.app.tg.cmd;

public class CommandsNamespace {
    //TODO: make it to be configurable via property?
    public static final String CMD_RESTART = "restart";
    public static final String CMD_RESTART_DESC = "Начать квест заново";

    public static final String CMD_START = "start";
    public static final String CMD_START_DESC = "Начать квест";

    public static final String CMD_NOT_FOUND_REPLY = "notfound";
    public static final String CMD_NOT_FOUND_REPLY_DESC = "Нет такой команды";

    public static final String CMD_INVITE = "invite";
    public static final String CMD_INVITE_DESC = "Пригласить пользователя";

    public static final String CMD_EXIT_TO_MAIN_MENU = "exit";
    public static final String CMD_EXIT_TO_MAIN_MENU_DESC = "Выйти в основное меню бота";

    public static final String CMD_EXEC_STEP = "exec";
    public static final String CMD_EXEC_STEP_DESC = "Выполнить действие";

    public static final String CMD_LAUNCH = "launch";
    public static final String CMD_LAUNCH_DESC = "Запустить квест для другого юзера";
}
