package com.pc.questbot.app.tg.cmd;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.Chat;
import org.telegram.telegrambots.api.objects.User;
import org.telegram.telegrambots.bots.AbsSender;
import org.telegram.telegrambots.exceptions.TelegramApiException;

import java.io.IOException;

/**
 * A command to invite other tg user to common ExpenseTemplate. If he accepts the link: he will switch his current expense template
 * to the inviter's (aka author) one
 */
@Component(CommandsNamespace.CMD_INVITE)
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
//TODO: create AbstractBotCommand class with helper and skeleton methods
public class InviteUserCommand extends AbstractBotCommand {

    @Value("${tg.bot.username}")
    private String botUsername;

    public InviteUserCommand() {
        super(CommandsNamespace.CMD_INVITE, CommandsNamespace.CMD_INVITE_DESC);
    }

    @Override
    protected void doExecute(AbsSender absSender, User user, Chat chat, String[] args) throws TelegramApiException, IOException {
        SendMessage answerMessage = htmlFormatted(buildAnswerText(), chat);
        SendMessage answerMessageLink = htmlFormatted(generateDeepLink(chat.getId().toString()), chat);

        absSender.execute(answerMessage);
        absSender.execute(answerMessageLink);
    }

    private String buildAnswerText() {
        return "<b>Хочешь</b> пригласить пользователя для ведения совместного бюджета? Не проблема.\n\nВот твоя уникальная ссылка для приглашения другого пользователя. " +
                "Перешли её тому, кого хочешь пригласить.\n\nПройдя по ссылке он немедленно получит доступ к совместному журналу трат:";
    }

    private String generateDeepLink(String authorIdentity) {
        return String.format("https://telegram.me/%s?start=%s", getBotUsername(), authorIdentity);
    }

    String getBotUsername() {
        return botUsername;
    }
}
