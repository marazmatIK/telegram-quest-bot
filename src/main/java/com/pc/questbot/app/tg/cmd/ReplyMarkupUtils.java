package com.pc.questbot.app.tg.cmd;

import com.pc.questbot.app.tg.InlineKeyboardButtonBuilder;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import org.telegram.telegrambots.bots.commandbot.commands.BotCommand;

import java.io.IOException;

public class ReplyMarkupUtils {

    public static InlineKeyboardButton button(String text, String command) {
        InlineKeyboardButton button = new InlineKeyboardButtonBuilder()
                .setText(text)
                .setCallbackData(command)
                .build();
        return button;
    }

    public static InlineKeyboardButton button(String text, BotCommand botCommand) throws IOException {
        InlineKeyboardButton button = new InlineKeyboardButtonBuilder()
                .setText(text)
                .setCallbackData(botCommand.getCommandIdentifier())
                .build();
        return button;
    }
}
