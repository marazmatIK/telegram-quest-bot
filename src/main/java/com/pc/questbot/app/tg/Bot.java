package com.pc.questbot.app.tg;

import com.pc.questbot.app.tg.cmd.BotCommandRegistry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.telegram.telegrambots.api.objects.Chat;
import org.telegram.telegrambots.api.objects.Update;
import org.telegram.telegrambots.api.objects.User;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.bots.DefaultBotOptions;

import java.util.Arrays;
import java.util.List;

public class Bot extends TelegramLongPollingBot {

    @Autowired
    private BotCommandRegistry botCommandRegistry;

    private final String botUsername;
    private final String token;

    public Bot(String botUsername, String token, DefaultBotOptions options) {
        super(options);
        this.botUsername = botUsername;
        this.token = token;
    } 

    public Bot(String botUsername, String token) {
        this.botUsername = botUsername;
        this.token = token;
    }

    @Override
    public String getBotUsername() {
        return botUsername;
    }

    @Override
    public String getBotToken() {
        return token;
    }

    @Override
    public void onUpdateReceived(Update update) {
        if (update.hasMessage() && update.getMessage().hasText()) {
            /*if (!stateBasedInputHandler.handle(update, this)) {
                executeCommandFromString(update.getMessage().getFrom(),
                        update.getMessage().getChat(), update.getMessage().getText());
            }*/
            executeCommandFromString(update.getMessage().getFrom(),
                    update.getMessage().getChat(), update.getMessage().getText());
        } else if (update.hasCallbackQuery()) {
            try {
                processCallbackQuery(update);
            } catch (Exception e) {
                //TODO: answer with error
                e.printStackTrace();
            }
        } else if (update.hasInlineQuery()) {
        }
    }

    //TODO: investigate some bug:  java.lang.NullPointerException at AddExpenseCommand.parseArgs(AddExpenseCommand.java:146)
    /** java.lang.NullPointerException
     at AddExpenseCommand.parseArgs(AddExpenseCommand.java:146)
     at AddExpenseCommand.doExecute(AddExpenseCommand.java:66)
     at AbstractBotCommand.execute(AbstractBotCommand.java:20)
     at Bot.executeCommandFromString(Bot.java:72)
     at Bot.processCallbackQuery(Bot.java:59)
     at Bot.onUpdateReceived(Bot.java:49)
     at java.util.ArrayList.forEach(ArrayList.java:1249)
     at org.telegram.telegrambots.generics.LongPollingBot.onUpdatesReceived(LongPollingBot.java:27)
     at org.telegram.telegrambots.updatesreceivers.DefaultBotSession$HandlerThread.run(DefaultBotSession.java:306)**/
    //
    private void processCallbackQuery(Update update) throws Exception {
        executeCommandFromString(update.getCallbackQuery().getFrom(),
                update.getCallbackQuery().getMessage().getChat(), update.getCallbackQuery().getData());
    }

    private void executeCommandFromString(User user, Chat chat, String data) {
        if (!StringUtils.isEmpty(data)) {
            String[] tokens = data.split(" ", -1);
            String cmd = tokens[0];
            List<String> tokensList = Arrays.asList(tokens);
            String[] args = new String[] {};
            if (tokensList.size() > 1) {
                args = tokensList.subList(1, tokensList.size()).toArray(new String[] {});
            }
            botCommandRegistry.getCommand(cmd).execute(this, user, chat, args);
        }
    }
}
