package com.pc.questbot.app.tg.cmd;

import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.commandbot.commands.BotCommand;

@Component
public class BotCommandRegistry {
    @Autowired
    private ApplicationContext context;

    /**
     * Get command with all required injected dependencies; indicated by its designator: either with slash forward in front or without one
     * For example: "/start" or "start" will produce start command BotCommand implementation
     * @param commandName BotCommand instance (Bean with Prototype scope)
     * @return
     */
    public BotCommand getCommand(String commandName) {
        try {
            BotCommand command = context.getBean(commandName.replaceFirst("[/]", ""), BotCommand.class);
            return command;
        } catch (NoSuchBeanDefinitionException ex) {
            ex.printStackTrace();
            BotCommand command = context.getBean(CommandsNamespace.CMD_NOT_FOUND_REPLY, BotCommand.class);
            return command;
        }
    }
}
