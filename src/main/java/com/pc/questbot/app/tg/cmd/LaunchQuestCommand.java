package com.pc.questbot.app.tg.cmd;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.Chat;
import org.telegram.telegrambots.api.objects.User;
import org.telegram.telegrambots.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import org.telegram.telegrambots.bots.AbsSender;
import org.telegram.telegrambots.exceptions.TelegramApiException;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import static com.pc.questbot.app.tg.cmd.ReplyMarkupUtils.button;

@Component(CommandsNamespace.CMD_LAUNCH)
//TODO: are we really need Prototype scope for Bot Commands?..
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class LaunchQuestCommand extends AbstractBotCommand {
    private static final String TARGET_USER_PROPS_PATH = "target.properties";

    private ClassLoader loader = Thread.currentThread().getContextClassLoader();
    private final Properties props = new Properties();

    {
        try(InputStream resourceStream = loader.getResourceAsStream(TARGET_USER_PROPS_PATH)) {
            props.load(new InputStreamReader(resourceStream, "UTF-8"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public LaunchQuestCommand() {
        super(CommandsNamespace.CMD_LAUNCH, CommandsNamespace.CMD_LAUNCH_DESC);
    }

    @Override
    protected void doExecute(AbsSender absSender, User user, Chat chat, String[] args) throws TelegramApiException, IOException {
        SendMessage answerMessage = htmlFormatted(buildAnswerText(), Long.parseLong(props.getProperty("tg.chat.id")));

        List<List<InlineKeyboardButton>> keyboard = new ArrayList<>();
        InlineKeyboardMarkup markup = new InlineKeyboardMarkup();
        keyboard.add(Arrays.asList(button("START", "/" + CommandsNamespace.CMD_START)));

        markup.setKeyboard(keyboard);
        answerMessage.setReplyMarkup(markup);

        absSender.execute(answerMessage);
    }

    private String buildAnswerText() {
        return props.getProperty("tg.invite.message");
    }
}
