package com.pc.questbot.app.tg.cmd;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.Chat;
import org.telegram.telegrambots.api.objects.User;
import org.telegram.telegrambots.bots.AbsSender;
import org.telegram.telegrambots.exceptions.TelegramApiException;

import java.io.IOException;

@Component(CommandsNamespace.CMD_NOT_FOUND_REPLY)
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class NotFoundCommand extends AbstractBotCommand {
    public NotFoundCommand() {
        super(CommandsNamespace.CMD_NOT_FOUND_REPLY, CommandsNamespace.CMD_NOT_FOUND_REPLY_DESC);
    }

    @Override
    protected void doExecute(AbsSender absSender, User user, Chat chat, String[] args) throws TelegramApiException, IOException {
        SendMessage answerMessage = htmlFormatted(buildAnswerText(), chat);
        absSender.execute(answerMessage);
    }

    private String buildAnswerText() {
        return "Прости. Не распознал команду.. Попробуй ещё раз, друг";
    }
}
