package com.pc.questbot.app.config;

import com.pc.questbot.app.db.PgInstance;
import com.pc.questbot.app.tg.Bot;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.telegram.telegrambots.exceptions.TelegramApiRequestException;
import org.telegram.telegrambots.generics.LongPollingBot;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.net.URISyntaxException;


@Configuration
@ComponentScan("com.pc.expensemeter.app.tg")
@TestPropertySource(locations = {"classpath:data.source.properties"})
@EnableTransactionManagement
public class TestConfig {

    @Value("${tg.bot.username}")
    private String botUsername;

    //public static LongPollingBot mockBot = mock(LongPollingBot.class);

    @PostConstruct
    public static void initDbInstance() throws IOException, URISyntaxException, InterruptedException {
        PgInstance.run();
        //TODO: dirty hack.. make some other solution..
        Thread.sleep(2400);
        PgInstance.applySchema();
        Thread.sleep(1500);
    }

    @Bean
    public LongPollingBot getBot() throws TelegramApiRequestException {
        LongPollingBot bot = new Bot(botUsername, "");
        return bot;
    }
}
