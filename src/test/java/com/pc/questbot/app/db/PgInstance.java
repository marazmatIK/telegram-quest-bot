package com.pc.questbot.app.db;

import com.opentable.db.postgres.embedded.EmbeddedPostgres;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Paths;

public class PgInstance {
    public static void run() throws IOException {
        Thread pgThread = new Thread(() -> {
            try {
                EmbeddedPostgres pg = EmbeddedPostgres.builder().setPort(5432).start();
                System.out.println("Port is: " + pg.getPort());
                System.out.println("User is: postgres");
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        pgThread.start();

    }

    public static void applySchema() throws IOException, URISyntaxException, InterruptedException {
        File path = Paths.get(ClassLoader.getSystemResource("init.db.sh").toURI()).toFile();
        String shellScriptPath = path.getAbsolutePath();

        ProcessBuilder processBuilder = new ProcessBuilder("sh", "-c", "/bin/sh " + shellScriptPath);
        processBuilder.directory(new File(path.getParent()));
        Process process = processBuilder.start();
        process.waitFor();
    }

    public static void main(String[] args) throws IOException {
        run();
    }
}
